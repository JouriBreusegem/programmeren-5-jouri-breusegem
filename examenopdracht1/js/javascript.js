const map = L.map('map').setView([51.2194475, 4.4024643], 12);

const baseMap = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

baseMap.addTo(map);

const request = new XMLHttpRequest();
request.open('get', 'json/clubs.json', true);
request.overrideMimeType("application/json");

request.onload = function(){
    let data = JSON.parse(this.response);
    
    const clubs = data.clubs.map(club => {
    L.marker([club.lat, club.long]).bindPopup(`
        <h2>${club.naam}</h2>
        <p><b>Straat:</b> ${club.adres}</p>
        <p><b>Plaats:</b> ${club.plaats}</p>
    `).openPopup().addTo(map);
    });
    
    const plaatsen = data.clubs.reduce((sums, club) => {
        sums[club.plaats] = (sums[club.plaats] || 0) + 1;
        return sums;
    }, {});
    
    let banner = document.getElementById('clubMenu');
    for(let plaats in plaatsen){
        let p = document.createElement('p');
        p.className = "clubMenuItem";
        p.innerHTML = `<b>${plaats}</b>: ${plaatsen[plaats]}`;
        banner.appendChild(p);
    }
}

request.send();