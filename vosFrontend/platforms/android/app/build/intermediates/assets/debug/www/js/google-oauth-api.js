// Enter an API key from the Google API Console:
//   https://console.developers.google.com/apis/credentials?project=_
// Enter an API key from the Google API Console:
//   https://console.developers.google.com/apis/credentials?project=_
var apiKey = 'AIzaSyAZeKvv1rvs4YyIgpGApqOXSR6Got2J5Nk';

// Enter a client ID for a web application from the Google API Console:
//   https://console.developers.google.com/apis/credentials?project=_
// In your API Console project, add a JavaScript origin that corresponds
//   to the domain where you will be running the script.
var clientId = '596050631029-60mc4gin197s3a6o82algi8l5vaqpoql.apps.googleusercontent.com';
// Enter one or more authorization scopes. Refer to the documentation for
// the API or https://developers.google.com/identity/protocols/googlescopes
// for details.
var scopes = 'profile';

var signinButton = document.getElementById('signin-button');
var signoutButton = document.getElementById('signout-button');

function handleClientLoad() {
    // Load the API client and auth library
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    // gapi.client.setApiKey(apiKey);
    gapi.auth2.init({
            'apiKey': apiKey,
            client_id: clientId,
            scope: scopes
        }).then(function() {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(signedIn);
            // Handle the initial sign-in state.
            signedIn();
            })
        .catch(function(e) {
            alert(e.details);
        });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        signinButton.style.display = 'none';
        signoutButton.style.display = 'block';
         makeApiCall();
     }
    else {
        signinButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function signin(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function signout(event) {
    gapi.auth2.getAuthInstance().signOut();
}

// Load the API and make an API call.  Display the results on the screen.
function makePeopleApiCall() {
    gapi.client.load('people', 'v1', function() {
        var request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });
        request.execute(function(resp) {
            var p = document.createElement('p');
            if (resp.names) {
                var name = resp.names[0].givenName;
            }
            else {
                var name = 'Geen naam gevonden';
            }
            p.appendChild(document.createTextNode('Hello, ' + name + '!'));
            document.getElementById('content').appendChild(p);
            // Toon het response object als JSON string
            var pre = document.createElement('pre');
            var feedback = JSON.stringify(resp, null, 4);
            pre.appendChild(document.createTextNode(feedback));
            document.getElementById('content').appendChild(pre);
        });
    });
}

function signedIn()
{
    // Note: In this example, we use the People API to get the current
    // user's name. In a real app, you would likely get basic profile info
    // from the GoogleUser object to avoid the extra network round trip.
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    localStorage.setItem('userEmail', profile.getEmail());
    vos.login(localStorage.getItem('userEmail'));
}

function showUserProfile(resp) {
    // Note: In this example, we use the People API to get the current
    // user's name. In a real app, you would likely get basic profile info
    // from the GoogleUser object to avoid the extra network round trip.
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    var h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode(profile.getId()));
    document.getElementById('content').appendChild(h1);
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode(profile.getName()));
    document.getElementById('content').appendChild(h2);
    var h3 = document.createElement('h3');
    h3.appendChild(document.createTextNode(profile.getGivenName()));
    document.getElementById('content').appendChild(h3);
    var h4 = document.createElement('h4');
    h4.appendChild(document.createTextNode(profile.getFamilyName()));
    document.getElementById('content').appendChild(h4);
    var img = document.createElement('img');
    img.setAttribute("src", profile.getImageUrl());
    document.getElementById('content').appendChild(img);
    var h5 = document.createElement('h5');
    h5.appendChild(document.createTextNode(profile.getEmail()));
    document.getElementById('content').appendChild(h5);
}
