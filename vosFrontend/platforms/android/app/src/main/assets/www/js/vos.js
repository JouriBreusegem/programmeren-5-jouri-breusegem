var vos = {
    'model': {
        loaded: false,
        identity: {},
        procedureList: {},
        organisationList: {},
        position: {},
        myLocation: {},
        set: function() {
            /**
             * plaats JSON bestanden in vos.model.en vos.model.in local storage
             */
            //window.localStorage.clear();
            //localStorage.removeItem('model');
            var model = JSON.parse(localStorage.getItem("model"));
            if (model === null) {
                $http('data/identity.json')
                    .get()
                    .then(function(data) {
                        vos.model.identity = JSON.parse(data);
                        var payload = {};
                        // procedures depend on Role (in uppercase)
                        var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                        return $http(fileName).get(payload);
                    })
                    .then(function(data) {
                        vos.model.procedureList = JSON.parse(data);
                        var payload = { 'id': 1 };
                        return $http('data/position.json').get(payload);
                    })
                    .then(function(data) {
                        vos.model.position = JSON.parse(data);
                        var payload = {};
                        return $http('data/organisationList.json').get(payload);
                    })
                    .then(function(data) {
                        vos.model.organisationList = JSON.parse(data);
                        vos.model.loaded = true;
                        localStorage.setItem('model', JSON.stringify(vos.model));
                        vos.controller['home']['index']();
                    })
                    .catch(function(data) {
                        vos.model.loaded = false;
                        localStorage.setItem('model', JSON.stringify(vos.model));
                    });
            }
            else {
                vos.model = model;
                vos.controller['home']['index']();
            }
        }
    },

    'controller': {
        'home': {
            'index': function() {
                vos.getPosition();
                vos.setMyLocation();
                // model samenstellen
                vos.render.identity('#home .tile.identity');
                // view getoond worden
                location.href = "#home-index";
            },
            'loggingIn': function() {
                signin();
                vos.login(localStorage.getItem('userEmail'));
            },
            'login': function() {
                vos.login(localStorage.getItem('userEmail'));
                vos.getPosition();
                vos.setMyLocation();
                vos.render.identity('#home .identity');
                window.location.assign("index.html#home-index");
            },
            'logout': function() {
                localStorage.removeItem('userEmail');
                signout();
                vos.logout();
                vos.getPosition();
                vos.setMyLocation();
                vos.render.identity('#home .identity');
                window.location.assign("index.html#home-index");
            },
        },
		'gas': {
			'index': function() {
				vos.getPosition();
                vos.setMyLocation();
                vos.render.identity('#gas .tile.identity');
				location.href = 'gas-index';
			},
			'detection': function() {
				vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('GASM');
                vos.navigateTo('view-procedure', 'GASM');
			},
			'evacuation': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('GASE');
                vos.navigateTo('view-procedure', 'GASE');
            }
		},
        'fire': {
            'index': function() {
                // model samenstellen
                vos.getPosition();
                vos.setMyLocation();
                vos.render.identity('#fire .tile.identity');
                // view getoond worden
                location.href = "#fire-index";
            },
            'detection': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('BM');
                vos.navigateTo('view-procedure', 'Brandmelding');
            },
            'evacuation': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('BREV');
                vos.navigateTo('view-procedure', 'Evacuatie');
            }
        },
        'emergency': {
            'index': function() {
                vos.getPosition();
                vos.setMyLocation();
                // model samenstellen
                vos.render.identity('#emergency .tile.identity');
                // view getoond worden
                location.href = "#emergency-index";
            },
            'detection': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('VNBN');
                vos.navigateTo('view-procedure', 'Vaststelling noodsituatie');
            }
        },
        'accident': {
            'index': function() {
                // model samenstellen
                vos.getPosition();
                vos.setMyLocation();
                vos.render.identity('#home .tile.identity');
                // view getoond worden
                location.href = "#fire-index";
            },
            'extra-muros': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('BM');
                vos.navigateTo('view-procedure', 'Brandmelding');
            },
            'work-accident': function() {
                vos.render.identity('#view-procedure .show-room.view');
                vos.render.procedure.make('BREV');
                vos.navigateTo('view-procedure', 'Evacuatie');
            }
        },
        'page': {
            'previous': function() {
                window.history.back();
            }
        }
    },
    'render': {
        'identity': function(querySelector) {
            var elem = document.querySelector(querySelector);
            elem.innerHTML = '';
            elem.appendChild(modernWays.makeTextElement(vos.model.identity.firstName + ' ' + vos.model.identity.lastName, 'h2'))
            elem.appendChild(modernWays.makeTextElement(vos.model.identity.function, 'h3'));
            elem.appendChild(modernWays.makeTextElement(vos.model.identity.mobile, 'h4'));
            elem.appendChild(modernWays.makeTextElement(vos.model.myLocation.name));
            elem.appendChild(modernWays.makeTextElement(vos.model.myLocation.street));
            elem.appendChild(modernWays.makeTextElement(vos.model.myLocation.phone));
            if (vos.model.identity.loggedIn) {
                elem.appendChild(modernWays.makeTextElement('aangemeld als ' + vos.model.identity.role));
                var buttonElement = modernWays.makeButton('Afmelden');
                buttonElement.setAttribute('name', 'uc');
                buttonElement.setAttribute('value', 'home/logout');
                elem.appendChild(buttonElement);

            }
            else {
                elem.appendChild(modernWays.makeTextElement('aangemeld als gast'));
                var buttonElement = modernWays.makeButton('Aanmelden');
                buttonElement.setAttribute('name', 'uc');
                buttonElement.setAttribute('value', 'home/loggingIn');
                elem.appendChild(buttonElement);
            }
            return elem;
        },
        'procedure': {
            'make': function(procedureCode) {
                var procedure = vos.model.procedureList.procedure.find(function(item) {
                    return item.code === procedureCode;
                });
                elem = vos.render.identity('#view-procedure .show-room');
                var step = document.createElement('DIV');
                step.setAttribute('class', 'step');
                step.appendChild(modernWays.makeHtmlTextElement(procedure.heading, 'h2'));
                var listElement = document.createElement('OL');
                listElement.setAttribute('class', 'index');
                procedure.step.forEach(function(item, index) {
                    var step = modernWays.makeHtmlTextElement(item.title, 'li');
                    if ("action" in item) {
                        var commandPanelElem = modernWays.makeCommandPanel();
                        item.action.forEach(function(item) {
                            commandPanelElem.appendChild(vos.render.procedure[item.code](item, procedure.title, procedure.code));
                        });
                        step.appendChild(commandPanelElem);
                    }
                    if ("list" in item) {
                        step.appendChild(vos.render.procedure['LIST'](item.list));
                    }
                    listElement.appendChild(step);
                });
                step.appendChild(listElement);
                elem.appendChild(step);
            },
            'TEL': function(item, message, procedureCode) {
                // Het telefoonnummer van directie, secretariaat, ... is afhankelijk van de plaats
                var phoneNumber = vos.getPhoneNumber(item.phoneNumber);
                if (vos.model.identity.loggedIn) {
                    var buttonElement = modernWays.makeTileButton('Tel', 'icon-phone');
                    buttonElement.addEventListener('click', function() {
                        vos.log('TEL', message, procedureCode);
                        phoneCall(phoneNumber);
                    });
                    return buttonElement;
                }
                else {
                    return modernWays.makeTextElement(item.code + ' ' + phoneNumber, 'P');
                }
            },
            'SMS': function(item, message, procedureCode) {
                // Het telefoonnummer van directie, secretariaat, ... is afhankelijk van de plaats
                var phoneNumber = vos.getPhoneNumber(item.phoneNumber);
                if (vos.model.identity.loggedIn) {
                    var buttonElement = modernWays.makeTileButton('Tel', 'icon-send');
                    buttonElement.addEventListener('click', function() {
                        vos.log('SMS', message, procedureCode);
                        vos.smsPrepare(phoneNumber, message);
                    });
                    return buttonElement;
                }
                else {
                    return modernWays.makeTextElement(item.code + ' ' + phoneNumber, 'P');
                }
            },
            'LIST': function(list) {
                var listElement = document.createElement('OL');
                listElement.setAttribute('class', 'index');
                list.forEach(function(item) {
                    listElement.appendChild(modernWays.makeHtmlTextElement(item.title, 'li'))
                });
                return listElement;
            }
        }
    },
    /**
     * Ga naar de floor met de opgegeven view id en toon de tekst
     * in title in het h1 element.
     *
     * @param {string} view text id van de floor die getoond moet worden.
     * @param {string} title tekst die in het h1 element geplaatst moet worden.
     */
    navigateTo: function(view, title) {
        window.location.href = '#' + view;
        var h1 = document.querySelector('#' + view + ' h1');
        if (title && h1) {
            h1.innerHTML = title;
        }
    },
    /**
     * De dichtsbijzijnde organisatie ophalen.
     * https://stackoverflow.com/questions/21279559/geolocation-closest-locationlat-long-from-my-position
     */
    setMyLocation: function() {
        vos.model.organisationList.forEach(function(item) {
            item.distanceFromMyLocation = modernWays.getDistanceFromLatLonInKm(
                vos.model.position.latitude, vos.model.position.longitude,
                item.latitude, item.longitude);
        });
        vos.model.organisationList.sort(function(a, b) {
            return a.distanceFromMyLocation - b.distanceFromMyLocation;
        });

        vos.model.organisationList.forEach(
            function(item) {
                document.getElementById("feedback").innerHTML = '';
                document.getElementById("feedback").innerHTML += item.distanceFromMyLocation + ' ' + item.longitude + ' ' + item.latitude + ' ' + item.name + '<br>';
            });
        vos.model.myLocation = vos.model.organisationList[0];
    },
    /**
     * Voorbereiding versturen sms.
     *
     * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
     * @param {string} messageText boodschap die verstuurd zal worden.
     */
    smsPrepare: function (number, messageText) {
        // number = '0486788723';
        var message = messageText + '\n' +
            vos.model.identity.firstName + ' ' + vos.model.identity.lastName + '\n' +
            vos.model.myLocation.name + '\n' +
            vos.model.myLocation.street + '\n' +
            vos.model.myLocation.postalCode + ' ' + vos.model.myLocation.city + '\n' +
            number;
        smsSend(number, message);
    },
    /**
     * Geolocatie van de telefoon ophalen
     *
     */
    getPosition: function() {
        var options = {
            maximumAge: 3600000,
            timeout: 6000,
            enableHighAccuracy: false
        }
        var onSuccess = function(pos) {
            vos.model.position.latitude = pos.coords.latitude.toFixed(4);
            vos.model.position.longitude = pos.coords.longitude.toFixed(4);
            //vos.setMyLocation();
            //render.identity('#identity');
            //view['home']['index']();
        };
        var onError = function(error) {
            // stel in op hoofdzetel
            vos.model.position.latitude = 51.1771;
            vos.model.position.longitude = 4.3533;
            //vos.setMyLocation();
            //render.identity('#identity');
            //view['home']['index']();
        };
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    },
    /**
     * Juiste telefoonnummer ophalen.
     *
     */
    getPhoneNumber: function(number) {
        var phoneNumber = number;
        switch (number) {
            case 'DV':
                phoneNumber = vos.model.myLocation.directie.phone;
                break;
            case 'secretariaat':
                phoneNumber = vos.model.myLocation.secretariaat.phone;
                break;
            case 'preventieadviseur':
                phoneNumber = vos.model.myLocation.preventieadviseur.phone;
                break;
        }
        return phoneNumber;
    },
    /**
     * nagaan of de gebruikersnaam en het paswoord in de personList zitten
     * dit is geen veilige manier om aan te melden en moet je niet in productie app gebruiken
     */
    login: function(userName) {
        if (userName) {
            $http('data/personList.json')
                .get()
                .then(function(responseText) {
                    var person = JSON.parse(responseText);
                    var userIdentity = person.list.find(function(item) {
                        return item.userName === userName
                    });
                    if (userIdentity) {
                        userIdentity.loggedIn = true;
                        // identity = JSON.parse(localStorage.getItem('identity'));
                        vos.model.identity = userIdentity;
                        var payload = {};
                        // procedures depend on Role (in uppercase)
                        var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                        $http(fileName).get(payload)
                            .then(function(data) {
                                vos.model.procedureList = JSON.parse(data);
                                localStorage.setItem('model', JSON.stringify(vos.model));
                                vos.controller['home']['index']();
                            });
                    }
                    else {
                        alert('ongeldige gebruikersnaam of paswoord');
                    }
                })
                .catch(function(message) {
                    alert(message);
                })
        }
        else {
            alert('Je bent niet meer aangemeld met Google!');
        }
    },
    /**
     * de property loggedIn van identity op false zetten
     * en role op GAST
     */
    logout: function() {
        $http('data/identity.json')
            .get()
            .then(function(responseText) {
                vos.model.identity = JSON.parse(responseText);
                var payload = {};
                // procedures depend on Role (in uppercase)
                var fileName = 'data/procedure' + vos.model.identity.role.toUpperCase() + '.json';
                return $http(fileName).get(payload);
            })
            .then(function(responseText) {
                vos.model.procedureList = JSON.parse(responseText);
                vos.model.loaded = true;
                localStorage.setItem('model', JSON.stringify(vos.model));
                vos.controller['home']['index']();
            })
            .catch(function(message) {
                alert(message);
            });
    },
    log: function (actionCode, message, procedureCode) {
        var payload = {
            userName: vos.model.identity.userName,
            email: vos.model.identity.email,
            role: vos.model.identity.role,
            procedureCode: procedureCode,
            procedureTitle: actionCode,
            stepTitle: message,
            callNumber: vos.model.identity.phoneHome,
            sendNumber: vos.model.identity.phoneHome,
            timeStamp: ''
        };
      
      if (modernWays.checkConnection() === 'No network connection') {
            var cache = new Array();
            if (window.localStorage.getItem('payload') != null) {
                cache = JSON.parse(localStorage.getItem('payload'));
            }
            cache.push(payload);
            window.localStorage.setItem('payload', JSON.stringify(cache));
            alert(JSON.stringify(localStorage.getItem('payload')));

        } else {
            $http('https://fremdez-dotnetcore-fremdez.c9users.io:8080/api/vos').post(payload);
        }
    },
    pushLogCache: function () {
        if (modernWays.checkConnection() != 'No network connection') {
            if (window.localStorage.getItem('payload') != null) {
                var cache = JSON.parse(localStorage.getItem('payload'));
                for (var i = 0; i < cache.length; i++) {
                    $http('https://fremdez-dotnetcore-fremdez.c9users.io:8080/api/vos').post(cache[i]);
                }
                window.localStorage.removeItem('payload');
            }
       }
    }
}

var dispatcher = function(e) {
    var elem = e.target; // element dat het event heeft afgevuurd
    if (elem.getAttribute('name') === 'uc') {
        var ucArray = elem.getAttribute('value').split('/');
        var entity = ucArray[0];
        var action = ucArray[1];
        if (vos.controller[entity][action]) {
            vos.controller[entity][action]();
        }
        else {
            alert('ongeldige url ' + elem.getAttribute('value'));
        }
    }
}

document.getElementById('tower').addEventListener('click', dispatcher, false);
vos.model.set();
//check for network and attempt to push cashed logs every 5 minutes
setInterval(function(){
    vos.pushLogCache();
}, 300000)


