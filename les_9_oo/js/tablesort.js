// constructor function
// Naam van functie in Pascalnotatie
function TableSort(tableId) {
    this.tableElement = document.getElementById(tableId);
    if (this.tableElement && this.tableElement.nodeName === 'TABLE') {
        this.prepare();
    }
}

TableSort.prototype.prepare = function() {
    // voeg een pijltje toe aan de tekst in de heading
    // add arrow up
    // default is ascending order
    var headings = Array.from(this.tableElement.tHead.rows[0].cells);
    // nodelist is a collection and not an array
    // convert first to array
    headings.map(function(item) {
        item.innerHTML += '<span>&nbsp;&nbsp;&uarr;</span>';
        item.className = 'asc';
    });
    //this.tableElement.addEventListener('click', this.eventHandler, true);
    this.tableElement.addEventListener("click", function(that) {
        return function(event) {
            that.eventHandler(event);
            return false;
        }
    }(this), false);}

TableSort.prototype.eventHandler = function(event) {
    if (event.target.tagName === 'TH') {
        // alert('kolomkop');
        this.sortColumn(event.target);
        this.sign();
       }
    else if (event.target.tagName === 'SPAN') {
        if (event.target.parentNode.tagName === 'TH') {
            if (event.target.parentNode.className == "asc") {
                event.target.parentNode.className = 'desc';
                event.target.innerHTML = "&nbsp;&nbsp;&darr;"
            } else {
                event.target.parentNode.className = 'asc';
                event.target.innerHTML = "&nbsp;&nbsp;&uarr;"
            };
        }
    }
}

TableSort.prototype.sortColumn = function(headerCell) {
    // Get cell data for column that is to be sorted from HTML table
    let rows = this.tableElement.rows;
    let alpha = [],
        numeric = [];
    let alphaIndex = 0,
        numericIndex = 0;
    let cellIndex = headerCell.cellIndex;
    for (var i = 1; rows[i]; i++) {
        let cell = rows[i].cells[cellIndex];
        let content = cell.textContent ? cell.textContent : cell.innerText;
        let numericValue = content.replace(/(\$|\,|\s)/g, "");
        if (parseFloat(numericValue) == numericValue) {
            numeric[numericIndex++] = {
                value: Number(numericValue),
                row: rows[i]
            }
        }
        else {
            alpha[alphaIndex++] = {
                value: content,
                row: rows[i]
            }
        }
    }
    let orderdedColumns = [];
    numeric.sort(function(a, b) {
        return a.value - b.value;
    });
    alpha.sort(function(a, b) {
        let aName = a.value.toLowerCase();
        let bName = b.value.toLowerCase();
        if (aName < bName) {
            return -1
        } else if (aName > bName){
            return 1;
        } else {
            return 0;
        }
      });
      // Reorder HTML table based on new order of data found in the col array
    orderdedColumns = numeric.concat(alpha);
    let tBody = this.tableElement.tBodies[0];
    for (let i = 0; orderdedColumns[i]; i++) {
        tBody.appendChild(orderdedColumns[i].row);
    }
}

TableSort.prototype.copyright = 'Jef Inghelbrecht';
TableSort.prototype.sign = function() {
    let numberOfCols = this.tableElement.tHead.rows[0].cells.length;
    let tfoot = document.createElement('TFOOT');
    let tr = document.createElement('TR');
    let col = document.createElement('TD');
    let text = document.createTextNode(this.copyright);
    col.appendChild(text);
    tr.appendChild(col);
    tr.style.backgroundColor = 'red';
    tr.style.col'white';
    tr.style.textAlign = 'center';
    col.setAttribute('colspan', numberOfCols);
    tfoot.appendChild(tr);
    this.tableElement.appendChild(tfoot)
}