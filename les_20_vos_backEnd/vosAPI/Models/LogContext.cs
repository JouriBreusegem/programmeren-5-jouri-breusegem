﻿using Microsoft.EntityFrameworkCore;

namespace vosAPI.Models
{
    public class LogContext : DbContext
    {
        public LogContext(DbContextOptions<LogContext> options)
            : base(options)
        {
        }

        public DbSet<Log> Log { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySql("server=164.132.231.13; user id=docent300;password=AUdHX9QZ!;port=3306;database=docent300;Sslmode=none");
        }
    }
}
