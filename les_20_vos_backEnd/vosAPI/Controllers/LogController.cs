﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using vosAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace vosAPI.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/vos")]
    public class LogController : Controller
    {
        private LogContext _context;

        public LogController(LogContext context)
        {
            _context = context;
        }
        
        // GET: api/<controller>
        [HttpGet]
        public List<Log> Get()
        {
            return _context.Log.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name= "GetLog")]
        public IActionResult Get(int id)
        {
            var item = _context.Log.FirstOrDefault(l => l.Id == id);
            if (item == null)
                return NotFound();
            return new ObjectResult(item);
        }

        // POST api/<controller>
        [HttpPost]
        public string Post([FromBody]Log item)
        {
            if (item == null)
                return BadRequest().ToString();

            item.TimeStamp = DateTime.Now;
            _context.Log.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetLog",
                new { id = item.Id}, item).ToString();
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody]Log value)
        {
            if (value == null || value.Id != id)
                return BadRequest();

            var log = _context.Log.FirstOrDefault(l => l.Id == id);

            if (log == null)
                return NotFound();

            log.UserName = value.UserName;

            _context.Log.Update(log);
            _context.SaveChanges();

            return new NoContentResult();
        }


        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var log = _context.Log.FirstOrDefault(l => l.Id == id);

            if (log == null)
                return NotFound();

            _context.Log.Remove(log);
            _context.SaveChanges();

            return new NoContentResult();
        }
    }
}
