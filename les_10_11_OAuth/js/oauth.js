var clientId = "221778113998-30oo441hqtoibojj3one9p6ckccq4um4.apps.googleusercontent.com";
var apiKey = "AIzaSyACcZ8QKor6MfLZPHoHRYd57mheVuoShvs";
var scopes = "profile";

var logoutButton = document.getElementById("logoutButton");
var loginButton = document.getElementById("loginButton");

function handleClientLoad() {
    gapi.load("client:auth2", initAuth);
}

function initAuth() {
    gapi.auth2.init({
        'apiKey': apiKey,
        client_id: clientId,
        scope: scopes
    }).then(function(){
        //Luistert voor log-in state veranderingen
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSinginStatus);
        //verwerkt de initiele log-in state
        updateSinginStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

        loginButton.addEventListener("click", login);
        logoutButton.addEventListener("click", logout);
    }).then(makePeopleApiCall).then(showUserProfile);
}

function updateSinginStatus(isSignedIn) {
    if (isSignedIn) {
        logoutButton.style.display = "block";
        loginButton.style.display = "none";
    }
    else {
        loginButton.style.display = "block";
        logoutButton.style.display = "none";
    }
}

function login(event){
    gapi.auth2.getAuthInstance().signIn().then(makePeopleApiCall).then(showUserProfile);
}

function logout(event){
    gapi.auth2.getAuthInstance().signOut().then(function(){
        document.getElementById('content').innerHTML = '';
    });
}

function makePeopleApiCall() {
    gapi.client.load('people', 'v1', function() {
        var request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });
        request.execute(function(resp) {
            var p = document.createElement('p');
            if (resp.names) {
                var name = resp.names[0].givenName;
            }
            else {
                var name = 'Geen naam gevonden';
            }
            p.appendChild(document.createTextNode('Hello, ' + name + '!'));
            document.getElementById('content').appendChild(p);
            // Toon het response object als JSON string
            var pre = document.createElement('pre');
            var feedback = JSON.stringify(resp, null, 4);
            pre.appendChild(document.createTextNode(feedback));
            document.getElementById('content').appendChild(pre);
        });
    });
}

function showUserProfile(resp) {
    // Note: In this example, we use the People API to get the current
    // user's name. In a real app, you would likely get basic profile info
    // from the GoogleUser object to avoid the extra network round trip.
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    var h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode(profile.getId()));
    document.getElementById('content').appendChild(h1);
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode(profile.getName()));
    document.getElementById('content').appendChild(h2);
    var h3 = document.createElement('h3');
    h3.appendChild(document.createTextNode(profile.getGivenName()));
    document.getElementById('content').appendChild(h3);
    var h4 = document.createElement('h4');
    h4.appendChild(document.createTextNode(profile.getFamilyName()));
    document.getElementById('content').appendChild(h4);
    var img = document.createElement('img');
    img.setAttribute("src", profile.getImageUrl());
    document.getElementById('content').appendChild(img);
    var h5 = document.createElement('h5');
    h5.appendChild(document.createTextNode(profile.getEmail()));
    document.getElementById('content').appendChild(h5);
}