class googleApp {
    constructor(params){
        this.apiKey = params.apiKey;
        this.clientId = params.clientId;
        this.scopes = params.scopes;
        this.loginButton = document.getElementById(params.loginButton);
        this.logoutButton = document.getElementById(params.logoutButton);
    }
    
    initAuth(){
        gapi.auth2.init({
            'apiKey': this.apiKey,
            client_id: this.clientId,
            scope: this.scopes
        }).then(() => function(){
            //Luistert voor log-in state veranderingen
            gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSinginStatus);
            //verwerkt de initiele log-in state
            this.updateSinginStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            //Events aan knoppen toevoegen
            this.loginButton.addEventListener("click", this.login);
            this.logoutButton.addEventListener("click", this.logout);
        }).then(()=>this.makePeopleApiCall).then(() => this.showUserProfile);
    }
    
    updateSinginStatus(isSignedIn) {
        if (isSignedIn) {
            this.logoutButton.style.display = "block";
            this.loginButton.style.display = "none";
        }
        else {
            this.loginButton.style.display = "block";
            this.logoutButton.style.display = "none";
        }
    }
    
    login(){
        gapi.auth2.getAuthInstance().signIn().then(() => this.makePeopleApiCall).then(() => this.showUserProfile);
    }
    
    logout(){
        gapi.auth2.getAuthInstance().signOut().then(function(){
            document.getElementById('content').innerHTML = '';
        });
    }
    
    makePeopleApiCall() {
        gapi.client.load('people', 'v1', function() {
            var request = gapi.client.people.people.get({
                resourceName: 'people/me',
                'requestMask.includeField': 'person.names'
            });
            request.execute(function(resp) {
                var p = document.createElement('p');
                if (resp.names) {
                    var name = resp.names[0].givenName;
                }
                else {
                    var name = 'Geen naam gevonden';
                }
                p.appendChild(document.createTextNode('Hello, ' + name + '!'));
                document.getElementById('content').appendChild(p);
                // Toon het response object als JSON string
                var pre = document.createElement('pre');
                var feedback = JSON.stringify(resp, null, 4);
                pre.appendChild(document.createTextNode(feedback));
                document.getElementById('content').appendChild(pre);
            });
        });
    }
    
    showUserProfile(resp) {
        // Note: In this example, we use the People API to get the current
        // user's name. In a real app, you would likely get basic profile info
        // from the GoogleUser object to avoid the extra network round trip.
        var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
        var h1 = document.createElement('h1');
        h1.appendChild(document.createTextNode(profile.getId()));
        document.getElementById('content').appendChild(h1);
        var h2 = document.createElement('h2');
        h2.appendChild(document.createTextNode(profile.getName()));
        document.getElementById('content').appendChild(h2);
        var h3 = document.createElement('h3');
        h3.appendChild(document.createTextNode(profile.getGivenName()));
        document.getElementById('content').appendChild(h3);
        var h4 = document.createElement('h4');
        h4.appendChild(document.createTextNode(profile.getFamilyName()));
        document.getElementById('content').appendChild(h4);
        var img = document.createElement('img');
        img.setAttribute("src", profile.getImageUrl());
        document.getElementById('content').appendChild(img);
        var h5 = document.createElement('h5');
        h5.appendChild(document.createTextNode(profile.getEmail()));
        document.getElementById('content').appendChild(h5);
    }
}